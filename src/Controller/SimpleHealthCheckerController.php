<?php

namespace Drupal\simplehealthchecker\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns the current time to check site health.
 */
class SimpleHealthCheckerController extends ControllerBase {

    /**
     * Health checker.
     *
     * @return Response
     *   The response containing the current time.
     */
    public function healthChecker(): Response {
        return new Response(
            (string) time(), Response::HTTP_OK, [
                'Content-Type' => 'text/plain',
            ]
        );
    }

}
