Simple Health Checker
===============

This module is compatible with Drupal 8, 9, and 10, offering a basic health checking feature.
It provides a simple endpoint for monitoring tools like Kibana, Grafana, Zabbix, and others can utilize to evaluate the health of your Drupal site.

### Enable

-  Download and enable the module

### Change route path

You can alter a route URL using the 'alterRoutes' method of the 'RouteSubscriberBase' class. 